# Phoenix on Docker Compose Starter

- Phoenix 1.5.1
- Node 12 LTS
- Elixir 1.10.3
- Erlang 22.3.2

Base image is Ubuntu Bionic (18.04 LTS) to match my personal deployment target.

## Setup

### 1. Build docker images

Run `./dockerfiles/build`

### 2. Create environment file

Create environment file with `touch .env`.

Be sure to add this file to your `.gitignore` after you generate the application in step 3.

Add any environment vars that you wish to pass to the app in `.env` with format `KEY=value`

### 3. Create Phoenix application

`docker-compose run web mix phx.new . --app hello` (replace `hello` with your application name)

### 4. Update the database connection details

You should replace the username and password in `docker-compose.yml` with something more secure.

Edit `hello/config/dev.esx`:

```
config :hello, Hello.Repo,
  username: "postgres",
  password: "postgres",
  database: "hello_dev",
  hostname: "db",
  show_sensitive_data_on_connection_error: true,
  pool_size: 10
```

### 5. Create the database

`docker-compose run web mix ecto.create`

## Running

Run the Phoenix application with `docker-compose up` (runs `mix phx.server`)
